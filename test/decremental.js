var Decremental = artifacts.require("./reverseauction/Decremental.sol");

contract('Decremental', (accounts) => {
    it("accepts a lower bid", () => {
        var decremental;

        return Decremental.deployed().then(function(instance) {
            decremental = instance;
        }).then(function() {
            decremental.bid(accounts[1], 10000);
            decremental.bid(accounts[2], 9000);

            return decremental.getLowestBid.call();
        }).then(function(result) {
            assert.equal(result, 9000, "The lowest bid is 9000.");
        });
    });

    it("gets the decrement amount", () => {
        var decremental;

        return Decremental.deployed().then(function(instance) {
            decremental = instance;
        }).then(function() {
            return decremental.getDecrementAmount.call();
        }).then(function(result) {
            assert.equal(result, 1000, "The decrement amount is 1000.");
        });
    });

    it("does not allow bids smaller than the decrement amount", () => {
        var decremental;

        Decremental.new(Date.now(), 1000).then(function (instance) {
            decremental = instance;
        }).then(function() {
            return decremental.bid(accounts[1], 10000);
            return decremental.bid(accounts[2], 9500);
        }).then(function(result) {
            assert.fail();
        }).catch(error => {
          assert.equal(error.message, "assert.fail()", "The bid amount must be equal to or greater than 1000.");
        });
    });
});
