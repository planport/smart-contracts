var InviteOnly = artifacts.require("./reverseauction/InviteOnly.sol");

contract('InviteOnly', (accounts) => {
    it("should add an invite", () => {
        var inviteOnly;

        return InviteOnly.deployed().then(function(instance) {
            inviteOnly = instance;
        }).then(function() {
            inviteOnly.addBidder(accounts[1]);

            return inviteOnly.getTotalInvites.call();
        }).then(function(result) {
            assert.equal(result, 1, "The number of invites is 1.");
        });
    });

    it("should not be able to bid because supplier is not on the list", () => {
        var inviteOnly;

        return InviteOnly.deployed().then(function(instance) {
            inviteOnly = instance;
        }).then(function() {
            return inviteOnly.bid(accounts[2], 10000);
        }).then(function(result) {
            assert.fail();
        }).catch(error => {
          assert.equal(error.message, "assert.fail()", "Auction should not be accepted because the supplier is not invited.");
        });
    });
});
