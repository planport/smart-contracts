var ReverseAuction = artifacts.require("./reverseauction/ReverseAuction.sol");

contract('ReverseAuction', (accounts) => {
    it("makes 2 bids and get the correct total", () => {
        var reverseAuction;

        return ReverseAuction.deployed().then(function(instance) {
            reverseAuction = instance;
        }).then(function() {
            reverseAuction.bid(accounts[1], 10000);
            reverseAuction.bid(accounts[2], 12000);

            return reverseAuction.getTotalBids.call();
        }).then(function(result) {
            assert.equal(result, 2, "The number of bids is 2.");
        });
    });

    it("gets the lowest bid.", () => {
        var reverseAuction;

        return ReverseAuction.deployed().then(function(instance) {
            reverseAuction = instance;
        }).then(function() {
            reverseAuction.bid(accounts[1], 10000);
            reverseAuction.bid(accounts[2], 12000);
            reverseAuction.bid(accounts[3], 9000);

            return reverseAuction.getLowestBid.call();
        }).then(function(result) {
            assert.equal(result, 9000, "The lowest bid is 9000.");
        });
    });

    it("does not allow bids after the auction has ended.", () => {
        var reverseAuction;

        ReverseAuction.new(Date.now()).then(function (instance) {
            reverseAuction = instance;
        }).then(function() {
            return reverseAuction.bid(accounts[1], 10000);
        }).then(function(result) {
            assert.fail();
        }).catch(error => {
          assert.equal(error.message, "assert.fail()", "Auction has ended and should not accept any more bids.");
        });
    });

    it("returns the winning bid.", () => {
        var reverseAuction;

        return ReverseAuction.deployed().then(function(instance) {
            reverseAuction = instance;
        }).then(function() {
            reverseAuction.setWinningBid(accounts[1]);
            return reverseAuction.getWinningBid();
        }).then(function(result) {
          assert.equal(result, accounts[1], "Winning bid is "+accounts[1]+".");
        });
    });

    it("gets a bid amount.", () => {
        var reverseAuction;

        return ReverseAuction.deployed().then(function(instance) {
            reverseAuction = instance;
        }).then(function() {
            reverseAuction.bid(accounts[1], 10000);

            return reverseAuction.getBidAmount.call(accounts[1]);
        }).then(function(result) {
            assert.equal(result, 10000, "The bid amount is 10000.");
        });
    });
});
