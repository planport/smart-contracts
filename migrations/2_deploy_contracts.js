var ReverseAuction = artifacts.require("./reverseauction/ReverseAuction.sol");
var InviteOnly = artifacts.require("./reverseauction/InviteOnly.sol");
var Decremental = artifacts.require("./reverseauction/Decremental.sol");

module.exports = function(deployer) {
    var endTime = Date.now() + 600000;
    deployer.deploy(InviteOnly, endTime).then(function() {
        return deployer.deploy(ReverseAuction, endTime);
    });

    deployer.deploy(Decremental, endTime, 1000).then(function() {
        return deployer.deploy(ReverseAuction, endTime);
    });
};
