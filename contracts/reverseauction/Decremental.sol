pragma solidity ^0.4.23;

import "./ReverseAuction.sol";

contract Decremental is ReverseAuction {
    uint decrementAmount;

    uint currentBid;

    constructor (uint _endTime, uint _decrementAmount) ReverseAuction(_endTime) public {
        decrementAmount = _decrementAmount;
    }

    function getDecrementAmount() public view returns (uint) {
        return decrementAmount;
    }

    function bid(address _bidder, uint _amount) public payable {
        require(_amount >= decrementAmount);

        ReverseAuction.bid(_bidder, _amount);
    }
}
