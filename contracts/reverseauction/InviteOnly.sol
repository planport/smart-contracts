pragma solidity ^0.4.23;

import "./ReverseAuction.sol";

contract InviteOnly is ReverseAuction {
    uint private totalInvitations = 0;

    mapping (address => bool) invitations;

    modifier bidderInvited(address _bidder) {
        require(isBidderInvited(_bidder), "Bidder not invited.");
        _;
    }

    constructor (uint _endTime) ReverseAuction(_endTime) public {
    }

    function addBidder(address _address) public onlyOwner {
        invitations[_address] = true;
        totalInvitations = totalInvitations + 1;
    }

    function isBidderInvited(address _address) public view returns (bool) {
        return invitations[_address];
    }

    function getTotalInvites() public view returns (uint) {
        return totalInvitations;
    }

    function bid(address _bidder, uint _amount) public payable bidderInvited(_bidder) {
        ReverseAuction.bid(_bidder, _amount);
    }
}
