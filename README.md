# Planport Smart Contracts

## Contract Structure

```
- ReverseAuction
```

## Development

The following covers how to set up the Planport Smart Contracts for further development.

These configuration notes are written for Linux-based systems. Modify for your own development environment.

### Setting up the Code

Planport Ethereum contract development uses the [Solidity](https://solidity.readthedocs.io/) programming language and leverages the [Truffle Framework](http://truffleframework.com/).

The smart contracts reference the following 3rd party libraries:

- [OpenZeppelin smart contracts library](https://github.com/OpenZeppelin/openzeppelin-solidity).

To set up the code on your local machine, fork the [Planport Smart Contracts repository](https://gitlab.com/planport/smart-contracts), then install the supporting libraries (assuming you have git and npm intalled along with the Truffle Framework):

```
git clone https://gitlab.com/planport/smart-contracts.git
npm install -E zeppelin-solidity@1.10.0
```

### Compiling and Testing

We use the unit test framework available with Truffle to thoroughly test Planport smart contracts. Currently, our environment uses truffle develop for local chain testing. Before running any unit tests, you will need to have truffle develop running.

From the command line, start develop then run test:

```
truffle develop
truffle test
```
